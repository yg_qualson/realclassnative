package com.realclassnative

import com.facebook.react.ReactActivity

class MainActivity : ReactActivity() {

    override fun getMainComponentName(): String? {
        return "RealclassNative"
    }
}
