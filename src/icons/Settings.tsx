import React from 'react'
import { Circle, Path, Svg } from 'react-native-svg'

const Settings = () => {
  return (
    <Svg width={20} height={20} viewBox='0 0 20 20' fill='none'>
      <Circle cx={10} cy={10} r={6.1} stroke='#6B727A' strokeWidth={1.8} />
      <Path
        d='M10 3V1M10 19v-2M14.675 5.792l1.653-1.653M4.156 16.311l1.653-1.653M17 10h2M1 10h2M14.675 14.659l1.653 1.652M3.747 3.696L5.4 5.349'
        stroke='#6B727A'
        strokeWidth={1.8}
        strokeLinecap='round'
      />
    </Svg>
  )
}

export default Settings
