import React, { createContext, useState } from 'react'
import { useContext } from 'react'
import { useEffect } from 'react'
import { Dimensions, Platform } from 'react-native'
import DeviceInfo from 'react-native-device-info'

/**
 * iPad
 * Compact: width <= 639, 일반 아이패드 가로모드 1/2 이하, 세로모드 split-view / slide-over
 * Regular: 678 <= width, 일반 아이패드 가로모드 2/3 이상, 아이패드 12.9 가로모드 1/2 이상, 아이패드 세로모드
 *
 * iPhone
 * Compact: 일반적인 아이폰 세로모드 및 가로모드
 * Regular: 가로모드의 width - (notch ? 88pt : 0 - margin 40pt) > 720 인 경우
 *
 * Android
 * Compact: width <= 720
 * Regular: width >  720
 */

export enum VIEW_TYPE {
  COMPACT = 'COMPACT',
  REGULAR = 'REGULAR',
}

function getViewType() {
  const { width } = Dimensions.get('window')

  if (Platform.OS === 'ios') {
    if (Platform.isPad) {
      if (width < 640) return VIEW_TYPE.COMPACT
      return VIEW_TYPE.REGULAR
    }

    // screenWidth - safeArea(88) - content margin(40)
    const availableWidth = width - (DeviceInfo.hasNotch() ? 128 : 0)
    if (availableWidth > 720) return VIEW_TYPE.REGULAR
    return VIEW_TYPE.COMPACT
  }

  // android or others
  if (width > 720) return VIEW_TYPE.REGULAR
  return VIEW_TYPE.COMPACT
}

const ViewTypeContext = createContext(getViewType())

export default function withViewType<P>(Component: React.ComponentType<P>) {
  return function WithViewType(props: P) {
    const [viewType, setViewType] = useState(getViewType())

    useEffect(() => {
      Dimensions.addEventListener('change', () => setViewType(getViewType()))
    }, [])

    return (
      <ViewTypeContext.Provider value={viewType}>
        <Component {...props} />
      </ViewTypeContext.Provider>
    )
  }
}

export function useViewType() {
  return useContext(ViewTypeContext)
}
