import 'react-native-gesture-handler'

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import { StatusBar, useColorScheme } from 'react-native'
import { SafeAreaProvider } from 'react-native-safe-area-context'

import withViewType from './contexts/viewType'
import Main from './views/Main'
import Study from './views/Study'

const Root = createStackNavigator()

const App = () => {
  const isDarkMode = useColorScheme() === 'dark'

  return (
    <SafeAreaProvider>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <NavigationContainer>
        <Root.Navigator headerMode='none'>
          <Root.Screen name='Main' component={Main} />
          <Root.Screen name='Study' component={Study} />
        </Root.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  )
}

export default withViewType(App)
