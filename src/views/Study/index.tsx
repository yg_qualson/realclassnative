import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'

import Clip from './Clip'
import Coaching from './Coaching'
import Lecture from './Lecture'
import Training from './Training'

const Stack = createStackNavigator()

const Study = () => {
  return (
    <Stack.Navigator
      // detachInactiveScreens
      screenOptions={{
        headerStyle: {
          backgroundColor: '#000',
        },
        animationEnabled: false,
      }}
    >
      <Stack.Screen name='Lecture' component={Lecture} />
      <Stack.Screen name='Clip' component={Clip} />
      <Stack.Screen name='Training' component={Training} />
      <Stack.Screen name='Coaching' component={Coaching} />
    </Stack.Navigator>
  )
}

export default Study
