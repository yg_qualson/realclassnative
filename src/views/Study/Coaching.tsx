import Voice, { SpeechResultsEvent } from '@react-native-voice/voice'
import React, { useEffect, useState } from 'react'
import { Button, PermissionsAndroid, Platform, Text } from 'react-native'
import AudioRecorderPlayer from 'react-native-audio-recorder-player'
import { SafeAreaView } from 'react-native-safe-area-context'
import RNFetchBlob from 'rn-fetch-blob'

import StudyNavigator from './StudyNavigator'

let audioRecorderPlayer = new AudioRecorderPlayer()
let result = ''

const Coaching: React.FC = () => {
  const [speech, setSpeech] = useState('')

  async function askRecording() {
    if (Platform.OS === 'android') {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        {
          title: 'Cool Photo App Camera Permission',
          message:
            'Cool Photo App needs access to your camera ' +
            'so you can take awesome pictures.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        }
      )
      console.log(granted, PermissionsAndroid.RESULTS)
      if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
        return false
      } else {
        return true
      }
    }
    return true
  }

  useEffect(() => {
    console.log(Voice)
    Voice.onSpeechStart = (e) => console.log(e)
    Voice.onSpeechError = (e) => console.log(e)
    Voice.onSpeechPartialResults = (e: SpeechResultsEvent) => {
      if (e.value) setSpeech(e.value[0])
    }
  }, [])

  async function checkPermissionAndStart() {
    const result = await askRecording()
    console.log(result)
    if (result) {
      Voice.start('en-US')
      onStartRecord()
    }
  }

  function stopRecord() {
    Voice.stop()
    onStopRecord()
  }

  const getPath = () => {
    const dirs = RNFetchBlob.fs.dirs
    const path = Platform.select({
      ios: 'hello.m4a',
      android: `${dirs.CacheDir}/hello.mp3`,
    })
    return path
  }

  const onStartRecord = async () => {
    audioRecorderPlayer = new AudioRecorderPlayer()
    result = await audioRecorderPlayer.startRecorder(getPath())
    audioRecorderPlayer.addRecordBackListener((e) => {
      console.log(e)
    })
    console.log(result)
  }

  const onStopRecord = async () => {
    result = await audioRecorderPlayer.stopRecorder()
    audioRecorderPlayer.removeRecordBackListener()
    console.log(result)
  }

  const onStartPlay = async () => {
    console.log('onStartPlay')
    const msg = await audioRecorderPlayer.startPlayer(result)
    console.log(msg)
  }

  const onPausePlay = async () => {
    await audioRecorderPlayer.pausePlayer()
  }

  const onStopPlay = async () => {
    console.log('onStopPlay')
    audioRecorderPlayer.stopPlayer()
    audioRecorderPlayer.removePlayBackListener()
  }

  return (
    <SafeAreaView>
      <StudyNavigator />
      <Text>코칭 화면</Text>
      <Button title='녹음 시작' onPress={checkPermissionAndStart} />
      <Text>STT result: {speech}</Text>
      <Button title='녹음 종료' onPress={stopRecord} />
      <Button title='녹음파일 재생' onPress={onStartPlay} />
    </SafeAreaView>
  )
}

export default Coaching
