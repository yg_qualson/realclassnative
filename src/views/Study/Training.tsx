import React from 'react'
import { Text } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'

import StudyNavigator from './StudyNavigator'

const Training: React.FC = () => {
  return (
    <SafeAreaView>
      <StudyNavigator />
      <Text>트레이닝 화면</Text>
    </SafeAreaView>
  )
}

export default Training
