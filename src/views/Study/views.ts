import { CompositeNavigationProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'

import { StudyNavigationProps } from '../views'

/**
 * VIEW TREE
 *   ├─ Study/        (Stack Navigator: 내부 이동시 계속 쌓임)
 *   │  ├─ Lecture
 *   │  ├─ Clip
 *   │  ├─ Training
 *   │  ├─ Coaching
 */

export type StudyParamList = {
  Lecture: undefined
  Clip: undefined
  Training: undefined
  Coaching: undefined
}

export type LectureNavigationProps = CompositeNavigationProp<
  StackNavigationProp<StudyParamList, 'Lecture'>,
  StudyNavigationProps
>
export type ClipNavigationProps = CompositeNavigationProp<
  StackNavigationProp<StudyParamList, 'Clip'>,
  StudyNavigationProps
>
export type TrainingNavigationProps = CompositeNavigationProp<
  StackNavigationProp<StudyParamList, 'Training'>,
  StudyNavigationProps
>
export type CoachingNavigationProps = CompositeNavigationProp<
  StackNavigationProp<StudyParamList, 'Coaching'>,
  StudyNavigationProps
>
