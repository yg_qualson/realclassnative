import { useNavigation } from '@react-navigation/core'
import { StackNavigationProp } from '@react-navigation/stack'
import React from 'react'
import { Button, View } from 'react-native'

import type { StudyViews } from '../views'

const StudyNavigator = () => {
  const navigation = useNavigation<StackNavigationProp<StudyViews>>()
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'center',
      }}
    >
      <Button title='1단계' onPress={() => navigation.replace('Lecture')} />
      <Button title='2단계' onPress={() => navigation.replace('Clip')} />
      <Button title='3단계' onPress={() => navigation.replace('Training')} />
      <Button title='4단계' onPress={() => navigation.replace('Coaching')} />
    </View>
  )
}

export default StudyNavigator
