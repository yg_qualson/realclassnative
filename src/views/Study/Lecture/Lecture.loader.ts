import axios from 'axios'
import React from 'react'
import { Platform } from 'react-native'
import { DRMType } from 'react-native-video'

// WARNING: THIS TOKEN MIGHT BE EXPIRED
export const RC_USER_TOKEN =
  'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjIwNzgyNzI3LCJqdGkiOiJlYjUzNDMzMDk1ZjU0YTliOTY0M2QzNzg4MzhjYzY0YSIsInVzZXJfaWQiOjkxLCJxX2xpY2Vuc2VfdHlwZSI6MTAsInFfbGljZW5zZV9pZCI6NTAwNjM4MSwicHJvZmlsZV9pZCI6MTQ0fQ.lGBBXW7hzbzbIqCwkN_tSb_H4QKpnKEzk6HoXtUj2mU'

export const DRM_FOR_LECTURE_URL =
  'https://api-v2.realclass.co.kr/v2/api/profiles/me/drm?media_type=LECTURE&id=7'

export const DRM_LECTURE_URL = {
  ios:
    'https://public.realclass.co.kr/video/lecture/4154283780e14afbaceaa0b0ed33ce9a/drm/hls/4154283780e14afbaceaa0b0ed33ce9a.m3u8',
  android:
    'https://public.realclass.co.kr/video/lecture/4154283780e14afbaceaa0b0ed33ce9a/drm/dash/4154283780e14afbaceaa0b0ed33ce9a.mpd',
}

export const NON_DRM_VIDEO =
  'https://public.realclass.co.kr/video/interview/1/tyler_beginner_720p.mp4'

export function useDRMToken(url: string) {
  const [token, setToken] = React.useState('')
  Platform
  React.useEffect(() => {
    axios
      .get(url, {
        method: 'GET',
        headers: { Authorization: `Bearer ${RC_USER_TOKEN}` },
        withCredentials: true,
      })
      .then((response) => response.data)
      .then((data) =>
        setToken(
          data[Platform.OS === 'ios' ? DRMType.FAIRPLAY : DRMType.WIDEVINE]
        )
      )
      .catch((error) => console.warn(error))
  }, [url])

  return token
}
