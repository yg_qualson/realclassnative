import { Platform } from 'react-native'

const Lecture = Platform.select({
  ios: () => require('./Lecture.ios'),
  android: () => require('./Lecture.android'),
  default: () => require('./Lecture.ios'),
})()

export default Lecture.default
