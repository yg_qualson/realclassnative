import axios from 'axios'
import React from 'react'
import { StyleSheet } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import Video, { DRMType } from 'react-native-video'

import StudyNavigator from '../StudyNavigator'
import {
  DRM_FOR_LECTURE_URL,
  DRM_LECTURE_URL,
  useDRMToken,
} from './Lecture.loader'

const Lecture: React.FC = () => {
  const token = useDRMToken(DRM_FOR_LECTURE_URL)

  if (!token) return null

  function getLicense(spcString: string) {
    const formData = new FormData()
    formData.append('spc', spcString)
    return axios
      .post<string>(`https://license.pallycon.com/ri/licenseManager.do`, {
        headers: { 'pallycon-customdata-v2': token },
        body: formData,
      })
      .then((response) => response.data)
  }

  return (
    <SafeAreaView>
      <StudyNavigator />
      <Video
        source={{ uri: DRM_LECTURE_URL.ios }}
        drm={{
          type: DRMType.FAIRPLAY,
          certificateUrl: 'https://public.realclass.co.kr/video/fairplay.cer',
          getLicense,
        }}
        controls
        style={styles.backgroundVideo}
      />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  backgroundVideo: {
    width: '100%',
    height: 280,
    backgroundColor: 'black',
  },
})

export default Lecture
