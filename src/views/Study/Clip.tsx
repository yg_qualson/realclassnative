import React from 'react'
import { Text } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'

import StudyNavigator from './StudyNavigator'

const Clip: React.FC = () => {
  return (
    <SafeAreaView>
      <StudyNavigator />
      <Text>영상 화면</Text>
    </SafeAreaView>
  )
}

export default Clip
