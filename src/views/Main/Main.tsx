import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import React from 'react'

import Account from './Account'
import MyClass from './MyClass/MyClass'
import StudyLog from './StudyLog'

const Tab = createBottomTabNavigator()
const Main = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name='MyClass' component={MyClass} />
      <Tab.Screen name='StudyLog' component={StudyLog} />
      <Tab.Screen name='Account' component={Account} />
    </Tab.Navigator>
  )
}

export default Main
