import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import { Button } from 'react-native-elements'

import SettingsIcon from '@/icons/Settings'

import { AccountNavigationProps } from '../views'
import MyAccount from './MyAccount/MyAccount'
import Settings from './Settings'
import { AccountParamList } from './views'

const Stack = createStackNavigator<AccountParamList>()

interface Props {
  navigation: AccountNavigationProps
}
const Account: React.FC<Props> = ({ navigation }) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#fff',
          borderBottomColor: '#eee',
          borderBottomWidth: 1,
        },
        headerRight: () => (
          <Button
            type='clear'
            icon={<SettingsIcon />}
            style={{ marginRight: 20 }}
            onPress={() =>
              navigation.navigate('Account', { screen: 'Settings' })
            }
          />
        ),
      }}
    >
      <Stack.Screen
        name='MyAccount'
        component={MyAccount}
        options={{ title: '내 정보' }}
      />
      <Stack.Screen
        name='Settings'
        component={Settings}
        options={{ title: '설정' }}
      />
    </Stack.Navigator>
  )
}

export default Account
