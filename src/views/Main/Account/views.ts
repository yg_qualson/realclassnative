import { CompositeNavigationProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'

import { AccountNavigationProps } from '../views'

/**
 * VIEW TREE
 *   │  ├─ Account/           (Stack Navigator: MyAccount 위에 Settings가 쌓일 수 있음)
 *   │  │  ├─ MyAccount
 *   │  │  ├─ Settings
 */
export type AccountParamList = {
  MyAccount: undefined
  Settings: undefined
}

export type MyAccountNavigationProps = CompositeNavigationProp<
  StackNavigationProp<AccountParamList, 'MyAccount'>,
  AccountNavigationProps
>

export type SettingsNavigationProps = CompositeNavigationProp<
  StackNavigationProp<AccountParamList, 'Settings'>,
  AccountNavigationProps
>
