import React from 'react'
import { ScrollView } from 'react-native'

import { MyAccountNavigationProps } from '../views'
import AccountSection from './sections/AccountSection'
import PurchasedSection from './sections/PurchasedSection'
import RewardsSection from './sections/RewardsSection'

interface Props {
  navigation: MyAccountNavigationProps
}
const MyAccount: React.FC<Props> = () => {
  return (
    <ScrollView>
      <AccountSection />
      <RewardsSection />
      <PurchasedSection />
    </ScrollView>
  )
}

export default MyAccount
