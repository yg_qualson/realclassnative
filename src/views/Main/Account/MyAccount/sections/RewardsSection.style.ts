import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  rewardsSection: {
    paddingHorizontal: 20,
  },
  shadowBox: {
    shadowColor: '#b7bdc6',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.5,
    shadowRadius: 14,
  },
  gradientBox: {
    paddingHorizontal: 20,
    borderRadius: 10,
  },
  moneyReward: {
    paddingVertical: 16,
  },
  pointReward: {
    paddingVertical: 16,
  },
  divider: {
    width: '100%',
    height: 1,
    backgroundColor: 'white',
    opacity: 0.4,
  },
})
