import React from 'react'
import { Text, View } from 'react-native'

import { useViewType } from '@/contexts/viewType'

import createStyle from './PurchasedSection.style'

const PurchasedSection: React.FC = () => {
  const viewType = useViewType()
  const styles = createStyle({ viewType })
  return (
    <View style={styles.purchasedSection}>
      <View style={styles.sectionTitleArea}>
        <Text style={styles.sectionTitle}>구매 상품</Text>
      </View>
      <View style={styles.purchaseList} />
    </View>
  )
}

export default PurchasedSection
