import React from 'react'
import { Text, View } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

import styles from './RewardsSection.style'

const RewardsSection = () => {
  return (
    <View style={styles.rewardsSection}>
      <View style={styles.shadowBox}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#53E897', '#6ADDC1']}
          style={styles.gradientBox}
        >
          <View style={styles.moneyReward}>
            <Text>챌린지 환급</Text>
            <Text>300,000원</Text>
          </View>
          <View style={styles.divider} />
          <View style={styles.pointReward}>
            <Text>리얼포인트</Text>
            <Text>1,000P</Text>
          </View>
        </LinearGradient>
      </View>
    </View>
  )
}

export default RewardsSection
