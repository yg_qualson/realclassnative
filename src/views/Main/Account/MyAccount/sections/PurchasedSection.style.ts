import { StyleSheet } from 'react-native'

import { VIEW_TYPE } from '@/contexts/viewType'

export default function createStyle({ viewType }: { viewType: VIEW_TYPE }) {
  return StyleSheet.create({
    purchasedSection: {
      width: '100%',
      paddingVertical: 40,
      paddingHorizontal: 20,
    },
    sectionTitleArea: {
      paddingBottom: 8,
      borderBottomWidth: 2,
      borderBottomColor: '#434449',
      marginBottom: viewType === VIEW_TYPE.COMPACT ? 24 : 32,
    },
    sectionTitle: {
      fontSize: 18,
      fontWeight: '700',
      color: '#434449',
    },
    purchaseList: {
      width: '100%',
      height: 30,
      backgroundColor: 'red',
    },
  })
}
