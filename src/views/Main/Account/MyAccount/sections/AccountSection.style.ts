import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  accountSection: {
    flexDirection: 'row',
    paddingVertical: 40,
    paddingHorizontal: 20,
  },
  profileImage: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginRight: 16,
    backgroundColor: 'red',
  },
})
