import React from 'react'
import { Button, Text, View } from 'react-native'

import styles from './AccountSection.style'

const AccountSection: React.FC = () => {
  return (
    <View style={styles.accountSection}>
      <View style={styles.profileImage} />
      <Text>{'반가워요\n리얼클래스님'}</Text>
      <Button title='개인정보 >' onPress={console.log} />
    </View>
  )
}

export default AccountSection
