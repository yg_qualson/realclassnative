import { BottomTabNavigationProp } from '@react-navigation/bottom-tabs'
import {
  CompositeNavigationProp,
  NavigatorScreenParams,
} from '@react-navigation/native'

import { MainNavigationProps } from '../views'
import { AccountParamList } from './Account/views'
import { StudyLogParamList } from './StudyLog/views'

/**
 * VIEW TREE
 *   ├─ Main/         (Bottom Tab Navigator: 하단 탭 선택에 따라 렌더링됨)
 *   │  ├─ MyClass
 *   │  ├─ StudyLog/
 *   │  ├─ Account/
 */

export type MainParamList = {
  MyClass: undefined
  StudyLog: NavigatorScreenParams<StudyLogParamList>
  Account: NavigatorScreenParams<AccountParamList>
}

export type MyClassNavigationProps = CompositeNavigationProp<
  BottomTabNavigationProp<MainParamList, 'MyClass'>,
  MainNavigationProps
>
export type StudyLogNavigationProps = CompositeNavigationProp<
  BottomTabNavigationProp<MainParamList, 'StudyLog'>,
  MainNavigationProps
>
export type AccountNavigationProps = CompositeNavigationProp<
  BottomTabNavigationProp<MainParamList, 'Account'>,
  MainNavigationProps
>
