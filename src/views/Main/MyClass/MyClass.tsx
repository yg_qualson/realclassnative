import React from 'react'
import { Button, Text } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'

import { MyClassNavigationProps } from '../views'

interface Props {
  navigation: MyClassNavigationProps
}
const MyClass: React.FC<Props> = ({ navigation }) => {
  return (
    <SafeAreaView>
      <Text>내 클래스 패이지</Text>
      <Button
        title='강의'
        onPress={() => navigation.navigate('Study', { screen: 'Lecture' })}
      />
      <Button
        title='영상'
        onPress={() => navigation.navigate('Study', { screen: 'Clip' })}
      />
      <Button
        title='트레이닝'
        onPress={() => navigation.navigate('Study', { screen: 'Training' })}
      />
      <Button
        title='코칭'
        onPress={() => navigation.navigate('Study', { screen: 'Coaching' })}
      />
    </SafeAreaView>
  )
}

export default MyClass
