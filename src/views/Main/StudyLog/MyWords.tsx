import React from 'react'
import { Text } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'

const MyWords: React.FC = () => {
  return (
    <SafeAreaView>
      <Text>내 단어</Text>
    </SafeAreaView>
  )
}

export default MyWords
