import { MaterialTopTabNavigationProp } from '@react-navigation/material-top-tabs'
import { CompositeNavigationProp } from '@react-navigation/native'

import { StudyLogNavigationProps } from '../views'

/**
 * VIEW TREE
 *   │  ├─ StudyLog/          (material-top-tabs: 상단 탭 상태에 따라 화면이 달라짐)
 *   │  │  ├─ MyReport
 *   │  │  ├─ MySentences
 *   │  │  ├─ MyWords
 *   │  │  ├─ MyCoaching
 */
export type StudyLogParamList = {
  MyReport: undefined
  MySentences: undefined
  MyWords: undefined
  MyCoachings: undefined
}

export type MyReportNavigationProps = CompositeNavigationProp<
  MaterialTopTabNavigationProp<StudyLogParamList, 'MyReport'>,
  StudyLogNavigationProps
>

export type MySentencesNavigationProps = CompositeNavigationProp<
  MaterialTopTabNavigationProp<StudyLogParamList, 'MySentences'>,
  StudyLogNavigationProps
>

export type MyWordsNavigationProps = CompositeNavigationProp<
  MaterialTopTabNavigationProp<StudyLogParamList, 'MyWords'>,
  StudyLogNavigationProps
>
export type MyCoachingsNavigationProps = CompositeNavigationProp<
  MaterialTopTabNavigationProp<StudyLogParamList, 'MyCoachings'>,
  StudyLogNavigationProps
>
