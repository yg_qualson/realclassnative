import {
  createMaterialTopTabNavigator,
  MaterialTopTabBar,
  MaterialTopTabBarProps,
} from '@react-navigation/material-top-tabs'
import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'

import MyCoachings from './MyCoachings'
import MyReport from './MyReport'
import MySentences from './MySentences'
import MyWords from './MyWords'

const Tabs = createMaterialTopTabNavigator()

const SafeAreaMaterialTopTabBar: React.FC<MaterialTopTabBarProps> = (props) => (
  <SafeAreaView
    style={{ backgroundColor: 'white' }}
    edges={['top', 'left', 'right']}
  >
    <MaterialTopTabBar {...props} />
  </SafeAreaView>
)

const StudyLog: React.FC = () => {
  return (
    <Tabs.Navigator tabBar={SafeAreaMaterialTopTabBar}>
      <Tabs.Screen name='학습내역' component={MyReport} />
      <Tabs.Screen name='내 문장' component={MySentences} />
      <Tabs.Screen name='내 단어' component={MyWords} />
      <Tabs.Screen name='내 코칭내역' component={MyCoachings} />
    </Tabs.Navigator>
  )
}

export default StudyLog
