import React from 'react'
import { Text } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'

const MySentences: React.FC = () => {
  return (
    <SafeAreaView>
      <Text>내 문장</Text>
    </SafeAreaView>
  )
}

export default MySentences
