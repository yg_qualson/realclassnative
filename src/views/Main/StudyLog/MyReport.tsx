import React from 'react'
import { Text } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'

const MyReport: React.FC = () => {
  return (
    <SafeAreaView>
      <Text>내 학습내역</Text>
    </SafeAreaView>
  )
}

export default MyReport
