import React from 'react'
import { Text } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'

const MyCoachings: React.FC = () => {
  return (
    <SafeAreaView>
      <Text>내 코칭내역</Text>
    </SafeAreaView>
  )
}

export default MyCoachings
