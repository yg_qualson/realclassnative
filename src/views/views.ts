import { NavigatorScreenParams } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'

import { MainParamList } from './Main/views'
import { StudyParamList } from './Study/views'

/**
 * VIEW TREE
 *   App/                 (Stack Navigator:  Main 위에 Study가 쌓일 수 있음)
 *   ├─ Main/
 *   ├─ Study/
 */
export type AppParamList = {
  Main: NavigatorScreenParams<MainParamList>
  Study: NavigatorScreenParams<StudyParamList>
}

export type MainNavigationProps = StackNavigationProp<AppParamList, 'Main'>
export type StudyNavigationProps = StackNavigationProp<AppParamList, 'Study'>
