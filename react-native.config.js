module.exports = {
  dependencies: {
    'react-native-video': {
      platforms: {
        android: {
          sourceDir: `${__dirname}/node_modules/react-native-video/android-exoplayer`,
          folder: `${__dirname}/node_modules/react-native-video`,
          packageImportPath: 'import com.brentvatne.react.ReactVideoPackage;',
          packageInstance: 'new ReactVideoPackage()',
        },
      },
    },
  },
}
